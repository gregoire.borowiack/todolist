#!/bin/bash
cd Frontend
docker build . -t todolist-frontend:latest
cd ..
cd Backend
docker build . -t todolist-backend:latest
#docker-compose down
docker-compose up -d