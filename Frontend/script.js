const taskUrl = 'http://todolist.loc'

let allTasks = []
let notDoneTasks = []
let doneTasks = []

function addTask(){

  // récupérer les valeur dans input
  let taskName = $("#newTaskName").val()
  let date = new Date($("#newTaskDate").val())
  let done = false
  let body = JSON.stringify({tache: taskName, date, done}) // JS -> JSON
  
  // faire le fetch POST
  let headers = {'Access-Control-Allow-Origin': '*',
              'Accept': 'application/json',
              'Content-Type': 'application/json'}
  if(confirm('Voulez vous ajouter la tache : '+ taskName + '?')){
    fetch(`${taskUrl}/task/`, {method: 'POST', headers:headers, body:body })
      .then(response => {
        response.json()
        console.log("réponse de la req POST", response)
        init()  
        alert('La tâche :'+ taskName +' est ajouée à la liste de tâches à faire') 
    })
    .catch(err => {
      console.log("erreur lors de la req POST", err)
    })
  } else {return}
}

function addElement(parentElement, task) {
  if (!task) {
    return
  }

  if(task.done ===false){
    console.log("date if :" +task.date)
    t1 = '<li><button onclick="deleteTask(\''+ task._id +'\')"> supprimer </button>' + 
    task.tache +" à faire le " +task.date.slice(0,10)+'<button onclick="updateTask(\''+ task._id +'\')"> Fait </button><button onclick="editTask(\''+ task._id +'\')">Modifier </button></li>'
    parentElement.append(t1)
  } else {
    console.log("date else",task.date)
    t1 = '<li><button onclick="deleteTask(\''+ task._id +'\')"> supprimer </button>' + 
    task.tache  + " à faire le " +task.date.slice(0,10)
    parentElement.append(t1)
  }


}



/**
 * Init function
 */
function init() {

  $('#notDoneTasks').empty()
  $('#doneTasks').empty()
  allTasks = []
  doneTasks = []
  notDoneTasks = []

  let headers = new Headers({
    'Access-Control-Allow-Origin': '*',
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  })

  fetch(`${taskUrl}/task`,{method: 'GET', headers: headers})
    .then(response =>{

    response.json().then(json => {
      allTasks = json.tasks
      for(let i = 0; i < allTasks.length ; i++) {
        if(allTasks[i]['done'] == true){
          doneTasks.push(allTasks[i])
        } else {
          notDoneTasks.push(allTasks[i])
        }
      }

      for(let i = 0; i < notDoneTasks.length; i++){
        addElement($('#notDoneTasks'), notDoneTasks[i])
      }

      for(let i = 0; i < doneTasks.length; i++){
        addElement($('#doneTasks'), doneTasks[i])
      }

    })
  })
  .catch(error => {
    console.error("IL Y A UNE ERREUR")
    $('#notDoneTasks').html(tasks)
  })
}

function deleteTask(id) {
  if(confirm('Voulez vous supprimer la tâche ?')){
    fetch(`${taskUrl}/task/${id}`,{method: 'DELETE'})
      .then(response => {
        init()
    })
    .catch(error => {
      console.log("erreur lors de la req DELETE",error)
    })
  } else {return}
}




function updateTask(id){

  if(confirm('La tâche selectionnée est faite ?')){
    let task = allTasks.find((elem) => {
      return elem._id === id
    })
    if (!task) {
      console.error('error')
      return
    }
    task.done = !task.done


    // faire le fetch put
    let headers = {'Access-Control-Allow-Origin': '*',
    'Accept': 'application/json',
    'Content-Type': 'application/json'}
  

    fetch(`${taskUrl}/task/${id}`,{method: 'PUT', headers:headers , body: JSON.stringify(task) })
    .then(response => {
      init()
    })
    .catch(error =>{
      console.log("erreur lors de la req PUT",error)
    })
  }else{return}
} 

function editTask(id){
  if(confirm('Modifier la tache sélectionnée ?')){

    let headers = {'Access-Control-Allow-Origin': '*',
    'Accept': 'application/json',
    'Content-Type': 'application/json'}
    let taskName = $("#newTaskName").val()

    let task = allTasks.find((elem) => {
      return elem._id === id
    })
    if (!task) {
      console.error('error')
      return
    }

    task.tache = taskName

    fetch(`${taskUrl}/task/${id}`, {method: 'PUT', headers:headers, body: JSON.stringify(task) })
    .then(response => {
      console.log("Tache modifiée")
      init()
    })
    .catch(error =>{
      console.log("erreur lors de la req PUT",error)
    })
    
  }
  else{return}
}


init()
