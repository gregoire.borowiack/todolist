const express = require('express')
const mongodb = require('mongodb')
const parser = require('body-parser')
// const { json } = require('body-parser')
const MongoClient = mongodb.MongoClient
const cors = require('cors')
console.log('APP JS ')
let db;

const mongoUrl = process.env.MONGO_URL || 'mongo'

MongoClient.connect(`mongodb://${mongoUrl}/`, function (err, database) {
    console.log('Connected')
    console.log('Error: ', err)
    if(!err) {
        db = database.db('mydb')
    }
})

const app = express()

app.use(parser.json()) // pas nécessaire si const { json }
app.use(cors({ origin: true }))

const allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}
app.use(allowCrossDomain);


const bodyPost = require('body-parser'); // Necessaire a la lecture des data dans le body de la requete (post)

app.use(bodyPost.json()); // support json encoded bodies
app.use(bodyPost.urlencoded({ extended: false })); // support encoded bodies
app.use((req, res, next) => {
    res.header('Content-Type', 'application/json');
    next();
})


console.log('///////////////////////////////////////////::')

// Liste des tâches 
app.get('/task',function(req,res){
    // on recherche tous les fichiers de la db avec .find()
    db.collection('tasks').find({}).toArray(function(err,result){
        if(err){
            res.status(500).end()
        } else {
            res.json({'success': true, 'tasks':result})
            
        } 
    }) 
});


// Ajoute une tâche
app.post('/task', function(req, res) {
    console.log("ajout d'une tâche")
    // Récupération des paramètres
    const item = req.body

    // Insertion dans la base de données
    db.collection('tasks').insertOne(item, function(err, result) {
        // Renvoi un résultat
        if(err){
            res.status(500)
            console.log("erreur lors de l'ajout" , err.message)
        } else {
            res.json({'success': true, 'tasks':result})
            console.log("insertion ok")
            //res.status(201).send(result.ops[0])
        } res.end()
    })
});

// Editer
app.put('/task/:id', function(req,res){
    // Récupérer l'ancienne valeur puis update par la nouvelle
    const query = { _id: new mongodb.ObjectID(req.params.id) }
    delete req.body._id
    console.log('BODY SANS ID',req.body)
    const newval = {$set : req.body}
    console.log('ON ENTRE BIEN DANS LE PUT')
    // Edition dans la db
    db.collection('tasks').updateOne(query, newval, function(err, result){
        if(err){
            console.log('IL Y A UNE ERREUR SERVEUR', err)
            res.status(500).end()
        } else {
            console.log(result)
            res.json({'success': true, 'tasks':result})
            //res.status(202).send(result)
        }
    })
});

// Supprimer une tâche
app.delete('/task/:id' , function(req,res){
    // Récupération de la tache à supprimer
    const query = { _id: new mongodb.ObjectID(req.params.id) }

    // suppression de l'element del de la DB
    db.collection('tasks').deleteOne(query, function(err, result){
            // Renvoi un résultat
            if(err){
                res.status(500).end()
            } else{
                if(result.result.n === 0) {
                    res.status(404).end()
                } else {
                    res.status(202).end()
                }
                
            }


    })



});



app.use(function(req, res, next){
    res.status(404).send('Page introuvable !');
});



app.listen(3000, function () {
console.log('Example app listening on port 3000!')
});